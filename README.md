# How to: Use I<sup>2</sup>S on Teensy 4.1 (PJRC's Audio Library)

Introduction to I<sup>2</sup>S on Teensy 4.1.

For a quick guide to Teensy 4.1 programming with Arduino IDE see https://gitlab.com/hzahnlei/how-to-teensy4.1-arduino.

Also take a look at the Audio System Design Tool for Teensy Audio Library: https://www.pjrc.com/teensy/gui/index.html.
It cannot only be used for creating audio code for your Teensy.
It also displays useful information like the Teensy pins to be used etc.

Generally I recommend to visit https://www.pjrc.com/teensy/.
The makers of Teensy provide plenty of useful information and tutorials.

## Prerequistes

- Teensyduino IDE 1.8.16 (https://www.pjrc.com/teensy/td_download.html)
- Teensy 4.1 board
- MAX98357A breakout board (Adafruit in my case)
- INMP441 breakout board (unbranded in my case)
- Micro-USB cable
- Breadboard
- Jumper wires
- Small speaker, 4 Ohm or 8 Ohm

## Playing Sounds from SD Card

Wire Teensy 4.1 and MAX98357A breakout board like shown below.
Also connect a small 4/8 Ohm speaker to the MAX98357A breakout board.

<img src="./sd_player_wiring.jpeg"/>

The pins are assigned relatively fixed by PJRC's audio library.
I was using the predefined pins, as suggested by the Teensy creator.

| Teensy 4.1 Pin | MAX98357A Pin | I<sup>2</sup>S     |
| -------------: | ------------: | :----------------- |
| 5.0V           | Vin           | n/a                |
| GND            | GND           | n/a                |
| 7 (RX/TX)      | DIN           | Serial data (SD)   |
| 20 (LRCLK)     | LRC           | Word select (WS)   |
| 21 (BCLK)      | BCLK          | Serial clock (SCK) |
| 23 (MCLK)      | n/a           | n/a                |

On a breadboard it sould look like this.
I have placed a tissue below the speaker silence vibrations of the speaker on the plastic breadboard.

<img src="./sd_player_breadboard.jpeg"/>

First prepare an SD card.
Either copy the provided `sd_player_sketch/gong.wav` to the root folder of the SD card, or provide your own WAV file (named `gong.wav`).
Supported are 16 bit at 44.1kHz.

Plug that SD card into the Teensy SD card slot.

Now, compile and upload `sd_player_sketch/sd_player_sketch.ino`.
You should hear a repeating pad sound once the upload is done.

## Recording Sound from MEMS Microphone

Wire the audio amp like shown above.
Additionally, wire a INMP441 breakout board according to wiring diagram and table below.

<img src="./mic_wiring.jpg"/>

| Teensy 4.1 Pin | INMP441 Pin | I<sup>2</sup>S     |
| -------------: | ----------: | :----------------- |
| 3.3V           | VDD         | n/a                |
| GND            | GND         | n/a                |
| 5 (RX/TX)      | SD          | Serial data (SD)   |
| 3 (LRCLK)      | WS          | Word select (WS)   |
| 4 (BCLK)       | SCK         | Serial clock (SCK) |
| 33 (MCLK)      | n/a         | n/a                |
| GND            | L/R         | n/a                |

On a breadboard it sould look like this.

<img src="./mic_breadboard.jpg"/>

Now, compile and upload `mic_to_amp_sketch/mic_to_amp_sketch.ino`.
You should hear your own voice when speaking into the microphone.

## Building a USB Audio Device

Attach I<sup>2</sup>S microphone and amplifier/speaker as shown above.
Compile and upload `usb_sketch/usb_sketch.ino`.

Please note, that you have to select `Tool/USB Type/Audio` from the Teensyduino menu.
Otherwise the sketch will not compile.
The compiler instead will complain that `AudioInputUSB does not name a type`.

<img src="./teensyduino_usb_audio.png"/>

Once in "USB Audio" mode, you need to press Teensys boot button before compiling an uploading.
Normally, when in serial mode, there is no need to press the boot button.
In this case simply click "compile and upload" in the Teensyduino IDE.

It will appear as USB audio in and audio out devices on your host computer once Teensy has been programmed.
The programmed Teensy implements a standard USB audio devices.
It is recognized out of the box on macOS and Ubuntu Linux without the need for installing drivers.

The following screenshots demonstrate how this look on macOS.
For example, iTunes can playback music through your breadboard speaker once you select "Teensy Audio" as output.
And audio can be recorded with Audacity for example.

<img src="./macos_select_audio.png"/>

<img src="./macos_usb_audio_in.png"/>

<img src="./macos_usb_audio_out.png"/>

The following screenshots show the audio prefences on Linux (Nvidia Jetson Nano).

<img src="./linux_usb_audio_in.jpg"/>

<img src="./linux_usb_audio_out.jpg"/>

This is a great example for how to write a good library.
I was able to build a USB audio in/out device without deep knowledge on I<sup>2</sup>S nor on USB.
It is intuitive, easy to use and does the job.
Thanks.
