/*
   Example that reads gong.wav from SD card and plays on left channel of I2S unit #1.

   The wave file has to be 16 bit at 44,100Hz as the Teensy audio library supports this only.

   According to this discussion thread (https://forum.pjrc.com/threads/46719-Finding-I2S-Pins-on-Pinout-Diagram) Paul
   Stoffregen, the creator of Teensy, states that the I2S pins are fixed. Paul Stoffregen: "The short answer is to just
   use the default pins and work the rest of your project around those I2S pins being consumed for digital audio. Using
   any other pins for I2S is quite hard to do, and even if you go to all that trouble, it will only buy you a very
   limited set of not-so-satisfying alternatives." Therefore, these are the Teensy 4.1 pins used:

   Pin    I2S function
   21     BCLK
   23     MCLK
    7     TX
   20     LRCLK
*/

#include <Audio.h>
#include <SD.h>

static constexpr auto LEFT_CHANNEL = 0U;
static constexpr auto RIGHT_CHANNEL = 1U;

AudioPlaySdWav sd_wav_player;
AudioOutputI2S i2s_output;
AudioAmplifier amp;
AudioConnection patch_sd_to_amp{sd_wav_player, LEFT_CHANNEL, amp, LEFT_CHANNEL};
AudioConnection patch_sd_to_amp2{sd_wav_player, RIGHT_CHANNEL, amp, RIGHT_CHANNEL};
AudioConnection patch_amp_to_output{amp, LEFT_CHANNEL, i2s_output, LEFT_CHANNEL};
AudioConnection patch_amp_to_output2{amp, RIGHT_CHANNEL, i2s_output, RIGHT_CHANNEL};

auto setup_audio() -> void
{
        AudioMemory(50);
        amp.gain(0.85);
}

auto setup_sd() -> void
{
        SD.begin(BUILTIN_SDCARD);
}

auto setup() -> void
{
        setup_audio();
        setup_sd();
}

auto loop() -> void
{
        sd_wav_player.play("gong.wav");
        delay(1000);
        while (sd_wav_player.isPlaying())
        {
                // Intentionally left blank.
        }
}
